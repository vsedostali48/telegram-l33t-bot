use chrono::NaiveDateTime;
use telegram_bot::*;

use std::sync::Mutex;

use diesel::prelude::*;
use diesel::SqliteConnection;

use lazy_static::*;

pub mod entities;
pub mod schema;

use crate::utils::get_username;
use entities::*;
use schema::scores::dsl as scores_dao;
use schema::users::dsl as users_dao;

use crate::errors::*;

lazy_static! {
    pub static ref DB_CONN: Mutex<SqliteConnection> =
        Mutex::new(SqliteConnection::establish("data/telegram-leet-bot.db").expect("Error connecting to sqlite3 db!"));
}

#[throws(LeetBotError)]
pub fn get_user(msg: &Message) -> Option<UserInfo> {
    let user_id = i64::from(msg.from.id);
    return get_user_by_id(user_id)?;
}

#[throws(LeetBotError)]
pub fn get_user_by_id(user_id: i64) -> Option<UserInfo> {
    let db = &DB_CONN.lock().unwrap();

    let user_by_id: Option<UserInfo> =
        users_dao::users.filter(users_dao::id.eq(user_id)).first(db.deref()).optional()?;

    return user_by_id;
}

#[throws(LeetBotError)]
pub fn insert_user(new_user_info: &UserInfo) {
    let db = &DB_CONN.lock().unwrap();
    diesel::insert_into(users_dao::users).values(&NewUser::from(new_user_info)).execute(db.deref())?;
}

#[throws(LeetBotError)]
pub fn update_user(user_info: &UserInfo) {
    let db = &DB_CONN.lock().unwrap();
    diesel::update(users_dao::users.filter(users_dao::id.eq(user_info.id))).set(user_info).execute(db.deref())?;
}

#[throws(LeetBotError)]
pub fn get_or_insert_user(msg: &Message) -> UserInfo {
    let saved_user = get_user(msg)?;
    match saved_user {
        Some(saved_user) => saved_user,
        None => {
            let new_user_info = UserInfo::new(i64::from(msg.from.id), &get_username(msg));
            insert_user(&new_user_info)?;
            new_user_info
        }
    }
}

#[throws(LeetBotError)]
pub fn insert_score(user: &UserInfo, chat_id: i64, text: &str, sent_time: NaiveDateTime, win: i64) -> ScoreInfo {
    let db = &DB_CONN.lock().unwrap();
    let score = NewScore::new(user.id, chat_id, text, sent_time, win);
    diesel::insert_into(scores_dao::scores).values(score).execute(db.deref())?;

    // retrieve inserted score with id
    let inserted_score = scores_dao::scores.order(scores_dao::id.desc()).first(db.deref())?;
    return inserted_score;
}

#[throws(LeetBotError)]
pub fn get_scores_for_user(user_id: i64) -> Vec<ScoreInfo> {
    let db = &DB_CONN.lock().unwrap();
    let saved_user = users_dao::users.filter(users_dao::id.eq(user_id)).first(db.deref())?;
    let scores = <ScoreInfo as BelongingToDsl<&UserInfo>>::belonging_to(&saved_user).load::<ScoreInfo>(db.deref())?;

    return scores;
}

#[throws(LeetBotError)]
pub fn get_scores_for_chat(chat_id: i64) -> Vec<ScoreInfo> {
    let db = &DB_CONN.lock().unwrap();
    let scores = scores_dao::scores.filter(scores_dao::chat_id.eq(chat_id)).load(db.deref())?;

    return scores;
}
