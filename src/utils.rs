use telegram_bot::*;

use chrono::{DateTime, Duration, Local, TimeZone, Timelike};
use tokio::time;

use std::fmt::{self, Display};

pub fn get_username(msg: &Message) -> String {
    if let Some(last_name) = &msg.from.last_name {
        return format!("{} {}", msg.from.first_name, last_name);
    }

    return msg.from.first_name.to_string();
}

/// Find longest common substring between two strings
/// Note: Taken from Rosetta Code Project
pub fn longest_common_substring(s1: &str, s2: &str) -> String {
    let s1_chars: Vec<char> = s1.chars().collect();
    let s2_chars: Vec<char> = s2.chars().collect();
    let mut lcs = "".to_string();

    for (i, c1) in s1_chars.iter().enumerate() {
        for (j, c2) in s2_chars.iter().enumerate() {
            if c1 == c2 {
                let tmp_lcs: String = s1_chars.iter().skip(i).zip(
                    s2_chars.iter().skip(j)
                )
                .take_while(|(c1, c2)| c1==c2)
                .map(|(c1, _)| c1)
                .collect();

                if tmp_lcs.len() > lcs.len() {
                    lcs = tmp_lcs;
                }
            }
        }
    }

    lcs
}

pub async fn wait_till_next_minute_start(now: DateTime<Local>) {
    let next_min_start = (now + Duration::seconds(60)).with_second(0).unwrap();
    let duration_to_next_min =
        next_min_start.signed_duration_since(now).to_std().expect("Must be convertable to standard duration");
    time::delay_for(duration_to_next_min).await;
}

pub trait LeetDate: Display {
    /// e.g., "20200102133700" for Jan 02 2020, 13:37:00 (TZ not needed)
    fn get_leet_match(&self) -> String;

    /// e.g., "1337" for Jan 02 2020, 13:37:00 (TZ not needed)
    fn get_leet_hhmm(&self) -> String;

    /// e.g. "2020-01-02 13:37:25.561 GMT" for Jan 02 2020, 13:37:25 GMT
    fn get_leet_date_time(&self) -> String;

    /// e.g. "13:37:25.561 GMT" for Jan 02 2020, 13:37:25 GMT
    fn get_leet_time(&self) -> String;

    // e.g. "13:37 GMT" for Jan 02 2020, 13:37:25 GMT
    fn get_leet_minute(&self) -> String;
}

impl<Tz: TimeZone> LeetDate for DateTime<Tz>
where
    Tz::Offset: fmt::Display,
{
    fn get_leet_match(&self) -> String {
        self.format("%Y%m%d%H%M%S").to_string()
    }

    fn get_leet_date_time(&self) -> String {
        self.format("%Y-%m-%d %H:%M:%S%.3f %Z").to_string()
    }

    fn get_leet_time(&self) -> String {
        self.format("%H:%M:%S%.3f %Z").to_string()
    }

    fn get_leet_minute(&self) -> String {
        self.format("%H:%M %Z").to_string()
    }

    fn get_leet_hhmm(&self) -> String {
        self.format("%H%M").to_string()
    }
}


#[cfg(test)]
mod test_substr {
    use super::longest_common_substring as lcs;

    #[test]
    fn smoke() {
        assert_eq!(lcs("123foo123", "QEWRQWERQfooTHRTH"), "foo");
        assert_eq!(lcs("foo123", "QEWRQWERQfooTHRTH"), "foo");
        assert_eq!(lcs("123123foo", "QEWRQWERQfooTHRTH"), "foo");
        assert_eq!(lcs("123123", "QEWRQWE"), "");
        assert_eq!(lcs("ОПАОПАпривОПА", "привАГААГА"), "прив");
        assert_eq!(lcs("ОПАОПАпривОПА", "при"), "при");
    }
}