use charming::{Chart, ImageRenderer, ImageFormat};
use charming::component::{Legend, Grid, Axis};
use charming::element::{AxisType, Emphasis, EmphasisFocus, TextStyle, AxisLabel, Orient};
use charming::series::{Series, bar};
use telegram_bot::*;

use chrono::{Local, NaiveDateTime, TimeZone, Timelike, NaiveDate, Utc, Days};
use chrono_tz::Tz;
use tz_search;

use futures::StreamExt;

use std::collections::HashSet;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::env;

use tokio::{runtime::Runtime, sync::mpsc::{channel, Receiver}};

#[macro_use]
extern crate diesel;

use lazy_static::lazy_static;

use log::*;

mod errors;
use errors::*;

mod database;
use database::entities::*;

mod matchers;

mod utils;
use utils::*;

lazy_static! {
    static ref BOT_API: Api = {
        let token = env::var("TELEGRAM_BOT_TOKEN").expect("TELEGRAM_BOT_TOKEN not set");
        Api::new(token)
    };
}

fn main() {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));
    let mut rt  = Runtime::new().unwrap();

    // Fetch new updates via long poll method
    rt.block_on(async {
        loop {
            let result = poll_bot_events().await;
            if let Err(error) = result {
                error!("Error while handling message: {}", error);
            }
        }
    })
}

/// Main bot operation loop
///
/// Basic flow looks like this:
/// 1. Receive update
/// 2. Check if it's a command, handle
/// 3. Check for possible leet message score
/// 4. If someone scored, notify reporting task
/// 5. Rinse and repeat
///
#[throws(LeetBotError)]
async fn poll_bot_events() {
    // setup a reporting task for sending statistics after scores are counted
    let (mut sender, receiver) = channel::<ScoreInfo>(30);
    tokio::spawn(async move { handle_status_update(receiver).await });

    let mut stream = BOT_API.stream();
    while let Some(update) = stream.next().await {
        // If the received update contains a new message...
        match update {
            Ok(Update { kind: UpdateKind::Message(msg), .. }) => {
                if let Message { kind: MessageKind::Text { ref data, .. }, .. } = msg {
                    info!("Got text message, chat {}: {}", msg.chat.id(), data);
                    if data.starts_with("/getloc") {
                        handle_getloc_command(&msg).await?;
                        continue;
                    }

                    if data.starts_with("/setloc") {
                        handle_setloc_command(&msg, data).await?;
                        continue;
                    }

                    if data.starts_with("/totals") {
                        handle_totals_command(&msg).await?;
                        continue;
                    }

                    if data.starts_with("/stats") {
                        handle_stats_command(&msg).await?;
                        continue;
                    }

                    // handle time match
                    let scored = handle_time_match(&msg, data).await;
                    if let Err(error) = scored {
                        // here all underlying errors are handled and reported
                        error!("Error while handling time match for message {}: {}", msg.id, error);
                        continue;
                    }

                    // time match resulted in a win for someone, send score
                    // to the reporting task
                    if let Ok(Some(score)) = scored {
                        debug!("Sending score to the reporting task: {:?}", score);
                        sender.send(score).await?;
                    }
                }
            }
            Ok(m) => info!("Got some update {:?}", m),
            Err(e) => error!("Failed to get updates: {:?}", e),
        }
    }
}

/// Task for reporting handler
///
/// This routine is responsible for sending stats message after "leet" minute has elapsed.
/// Multiple users may hit a score in the same minute, and bot replies are then deleted,
/// so we still need to put summary message after scores were counted.
///
/// **Arguments**:
/// * `score_receiver`: async MSPC chanel receiver for scores from the last minute
///
async fn handle_status_update(mut score_receiver: Receiver<ScoreInfo>) {
    info!("Reporter task initialized.");
    loop {
        let poll_result = score_receiver.recv().await;
        if poll_result.is_none() {
            // receiver returns None only if stream is exhausted
            // meaning runtime is finishing operation and sender was dropped
            return;
        }

        // await is handled, this means we have poll result ready and safe to unwrap
        let first_score = poll_result.unwrap();

        // shouldn't really happen, user is created for each score
        let first_score_user = database::get_user_by_id(first_score.user_id);
        if let Err(db_err) = first_score_user {
            error!("Error while retrieving user {} for score: {}", first_score.user_id, db_err);
            continue;
        }

        let first_score_user = first_score_user.unwrap();
        let current_minute = Local::now();
        let current_minute_str = current_instant_for_user(&first_score_user, &first_score.message_timestamp).get_leet_minute();

        // populate initial scoring message
        info!("Scheduling report for {}", current_minute_str);
        let mut scores_to_report = format!("Stats for last minute ({}):\n", current_minute_str);
        if let Some(user) = first_score_user {
            let first_score_leet = current_instant_for_user_timezone(&user, &first_score.message_timestamp);
            scores_to_report.push_str(&format!(
                "\t[{}](tg://user?id={}) scored {} at {}\n",
                user.username, user.id, first_score.score, first_score_leet.get_leet_date_time()
            ));
        }

        // reporting should happen only in next minute
        wait_till_next_minute_start(current_minute).await;
        while let Ok(score) = score_receiver.try_recv() {
            let saved_user = database::get_user_by_id(score.user_id);
            if let Some(user) = saved_user.unwrap_or(None) {
                let score_leet = current_instant_for_user_timezone(&user, &score.message_timestamp);
                scores_to_report.push_str(&format!(
                    "\t[{}](tg://user?id={}) scored {} at {}\n",
                    user.username, user.id, score.score, score_leet.get_leet_date_time()
                ));
            }
        }

        let result = BOT_API
            .send(SendMessage::new(ChatId::from(first_score.chat_id), scores_to_report).parse_mode(ParseMode::Markdown))
            .await;

        if let Err(error) = result {
            error!("Error while reporting stats for {}: {}", current_minute_str, error);
        }
    }
}

/// Handler for the `/getloc` chat command
///
/// Finds current timezone for the user and reports is as a reply.
///
/// **Arguments**:
/// * `msg`: Text message with `/getloc` command
///
#[throws(LeetBotError)]
async fn handle_getloc_command(msg: &Message) {
    let user = database::get_or_insert_user(msg)?;
    let tz_string = user.timezone.unwrap_or(String::from("not set yet"));

    BOT_API.send(
        msg.text_reply(format!("Current timezone for user `{}` is {}", user.username, tz_string))
            .parse_mode(ParseMode::Markdown)
    ).await?;
}

/// Handler for the `/setloc` chat command
///
/// Initially all users in a chat are assumed to have local timezone, i.e. native to the server
/// where bot is being run. However, people may see l33t time from all places on Earth, and this
/// does mean we should have a way to store timezone associated with the user.
///
/// Currently it works like this: user sends his location and then replies to it
/// with text message containing `/setloc` bot command.
///
/// **Arguments**:
/// * `msg`: Text message with `/setloc` command
///
#[throws(LeetBotError)]
async fn handle_setloc_command(msg: &Message, text: &str) {
    // we don't need to check owner of Location message, multiple people may want to run
    // `/setloc` on a message sent just by one person
    if let Some(MessageOrChannelPost::Message(Message { kind: MessageKind::Location { ref data }, .. })) =
        msg.reply_to_message.as_deref()
    {
        // this is a proper reply to a Location message, set timezone for this user
        let mut user = database::get_or_insert_user(msg)?;
        let tz_string = tz_search::lookup(data.latitude.into(), data.longitude.into()).expect("valid coordinates");
        let prev_tz_string = user.timezone.unwrap_or(String::from("N/A"));

        tz_string.parse::<Tz>().expect("Extracted timezone must be IANA-compliant");
        info!("Setting user {} (id {}) timezone to {} from {}", user.username, msg.from.id, tz_string, prev_tz_string);
        user.timezone = Some(tz_string.to_owned());
        database::update_user(&user)?;

        BOT_API
            .send(
                msg.text_reply(format!("Timezone for user `{}` changed to `{}`", user.username, tz_string))
                    .parse_mode(ParseMode::Markdown),
            )
            .await?;
    } else if let Ok(tz) = text.split(" ").nth(1).unwrap_or_default().parse::<Tz>() {
        // first argument after command is a valid timezone
        // examples: "setloc Europe/Berlin", "setloc@bot_name Europe/Moscow"
        let mut user = database::get_or_insert_user(msg)?;
        let prev_tz_string = user.timezone.unwrap_or(String::from("N/A"));

        info!("Setting user {} (id {}) timezone to {} from {}", user.username, msg.from.id, tz.name(), prev_tz_string);
        user.timezone = Some(tz.name().to_string());
        database::update_user(&user)?;

        BOT_API
            .send(
                msg.text_reply(format!("Timezone for user {} changed to {}", user.username, tz.name()))
                    .parse_mode(ParseMode::Markdown),
            )
            .await?;
    } else {
        // send usage hint
        BOT_API.send(msg.text_reply("`/setloc` call must either have valid timezone as a parameter or be a reply to a location-type message")
            .parse_mode(ParseMode::Markdown)
        ).await?;
    }
}

/// Performs broad search for match over all timezones
///
/// A user may try to score but without knowing the leet match
/// should be timezone-aware. This check will help him to understand
/// what `/setloc` is for and how he should use it.
///
/// **Arguments**:
/// * `msg`: Message that this bot received, possible leet score
/// * `text`: Text of the message, containing possible score
///
async fn handle_possible_tz_match(msg: &Message, text: &str) {
    let sent_time = NaiveDateTime::from_timestamp_opt(msg.date, 0).expect("Message date must be valid");

    // iterate over all possible timezones
    // we can have multiple timezones matching one string, so we need to collect them all
    let mut reply = String::from("This could be a l33t match if you were in other timezone, want to change your location? Write one of following commands:\n\n");
    let mut win_sum = 0;
    for tz in chrono_tz::TZ_VARIANTS.iter() {
        let sent_with_tz = tz.from_utc_datetime(&sent_time);
        let win = check_match_instant(&sent_with_tz, text);
        if win > 0 {
            win_sum = win_sum + win;
            reply.push_str(&format!("`/setloc {}`  \n", tz.name()));
        }
    }

    // If we have at least one matching timezone, reply with instructions
    if win_sum > 0 {
        let res = BOT_API.send(msg.text_reply(reply).parse_mode(ParseMode::Markdown)).await;
        match res {
            Ok(response) => info!("Sent tz hint to {} as message {}", msg.id, response.to_message_id()),
            Err(error) => error!("Couldn't send tz hint to {}: {}", msg.id, error),
        }
    }
}

/// performs check if a minute ago this would've been a score
///
/// A user may try to score a bit late so we could encourage him
/// not to lose hope for the next time.
///
/// **Arguments**:
/// * `msg`: Message that this bot received, possible late leet score
/// * `text`: Text of the message, containing possible score
///
async fn handle_possible_late_match(msg: &Message, text: &str) {
    let saved_user = database::get_user(msg);
    if !saved_user.is_ok() {
        error!("Database error while retrieving user {}", msg.from.id);
        return;
    }

    // check match for a minute ago
    let minute_ago = NaiveDateTime::from_timestamp_opt(msg.date - 60, 0).expect("Last minute from msg must be valid time");
    let attempt = current_instant_for_user(&saved_user.unwrap(), &minute_ago);
    let score = check_match_instant(attempt.as_ref(), text);
    if score == 0 {
        // not late match at all
        return;
    }

    // it was a late match, report it
    let actual_time = NaiveDateTime::from_timestamp_opt(msg.date, 0).expect("Message timestamp must be valid");
    let miss_duration = actual_time.signed_duration_since(actual_time.with_second(0).unwrap());
    let reply = format!(
        "Sorry you're a bit late!\nYou would score if you wrote this *{}* seconds ago. Don't lose hope!",
        miss_duration.num_milliseconds() as f64 / 1000.0
    );

    let res = BOT_API.send(msg.text_reply(reply).parse_mode(ParseMode::Markdown)).await;
    match res {
        Ok(response) => info!("Sent late hint to {} as message {}", msg.id, response.to_message_id()),
        Err(error) => error!("Couldn't send late hint to {}: {}", msg.id, error),
    }
}

/// Prints stats for the chat of this message
#[throws(LeetBotError)]
async fn handle_totals_command(msg: &Message) {
    let user_id = i64::from(msg.from.to_user_id());
    let chat_id = i64::from(msg.chat.id());
    info!("Handling /totals command in chat {} from user {}", chat_id, user_id);

    let all_scores_for_chat = database::get_scores_for_chat(chat_id)?;
    let score_graph = attach_stat_chart(&all_scores_for_chat)?;

    // compute this chat users scores
    let mut scores_by_user = HashMap::new();
    for win in &all_scores_for_chat {
        // update score
        let user = database::get_user_by_id(win.user_id)?.expect("User must be resolved");
        let sum = *scores_by_user.get(&user).unwrap_or(&0);
        scores_by_user.insert(user, sum + win.score);
    }

    // collect and sort all
    let mut scores_vec: Vec<(UserInfo, i64)> =
        scores_by_user.into_iter().map(|(user, score_sum)| (user, score_sum)).collect();
    scores_vec.sort_by_key(|item| item.1);
    scores_vec.reverse();

    let mut totals_string = String::from("*Total stats for this chat:*\n\n");
    for (idx, (user_info, score)) in scores_vec.into_iter().enumerate().rev() {
        totals_string.push_str(&format!(
            "{}. {} with *{}*\n",
            idx + 1,
            user_info.username,
            score
        ));
    }

    // send the totals message
    let response = BOT_API.send(
        msg.photo_reply(InputFileUpload::with_path(score_graph)).caption(totals_string).parse_mode(ParseMode::Markdown)
    ).await?;
    let totals_message_id = response.to_message_id();
    info!("Sent /totals command response, message {}", totals_message_id);
}

#[throws(LeetBotError)]
fn attach_stat_chart(all_scores_for_chat: &Vec<ScoreInfo>) -> &str {
    info!("Calculating stats graph points");

    // only leave scores which date back up to 30 days
    // would be a better way to do that via sqlite select, isn't it?
    let date_30_days_before = Utc::now().date_naive().checked_sub_days(Days::new(30)).unwrap();

    // 30 days before current date
    let days = date_30_days_before.iter_days().take(30).collect::<Vec<NaiveDate>>();
    let days_str = days.iter().map(|it| it.format("%Y-%m-%d").to_string()).collect();

    // generate chart skeleton
    // font_weight and font_family don't have any effect for now
    // see https://github.com/RazrFalcon/resvg/issues/480 for more info
    let mut chart = Chart::new()
        .background_color("white")
        .grid(Grid::new().contain_label(true))
        .legend(Legend::new()
                .orient(Orient::Vertical)
                .right("right")
                .padding(16)
                .text_style(TextStyle::new()
                            .font_size(16)
                            .font_weight("bolder")))
        .x_axis(Axis::new()
                .name("Day")
                .type_(AxisType::Category)
                .axis_label(AxisLabel::new()
                            .rotate(45)
                            .font_size(18))
                .name_text_style(TextStyle::new()
                            .font_size(20)
                            .font_weight("bolder"))
                .data(days_str))
        .y_axis(Axis::new()
                .name("Scores total")
                .type_(AxisType::Value)
                .axis_label(AxisLabel::new()
                            .font_size(18))
                .name_text_style(TextStyle::new()
                            .font_size(20)
                            .font_weight("bolder")));

    // all user ids that scored for the last months
    let user_ids = all_scores_for_chat.iter()
        .filter(|it| it.message_timestamp.date() > date_30_days_before)
        .map(|it| it.user_id)
        .collect::<HashSet<i64>>();

    // for each user collect scores for every day from 30 days before
    // TODO: make this more memory efficient
    for user_id in user_ids.into_iter() {
        let username = database::get_user_by_id(user_id)?.expect("User must be resolved").username;
        let scores_data = days.iter()
            .map(|day| all_scores_for_chat.iter()
                 .filter(|score| score.user_id == user_id)
                 .filter(|score| score.message_timestamp.date() == *day)
                 .count() as i32)
            .collect::<Vec<i32>>();

        info!("{}: {:?}", username, scores_data);
        chart = chart.series(Series::Bar(bar::Bar::new()
                    .name(username)
                    .stack("Users")
                    .emphasis(Emphasis::new().focus(EmphasisFocus::Series))
                .data(scores_data)));
    }

    info!("Writing stats chart to the file");
    let mut renderer = ImageRenderer::new(1920, 1080);
    renderer.save_format(ImageFormat::Png, &chart, "/tmp/chart.png")?;

    return "/tmp/chart.png";
}

/// Prints stats for the user of this message
///
/// Should show:
/// * How much did this user scored totally
/// * How much did this user score in current chat
/// * When was the last time this user scored in this chat
/// * When was the first time this user scored in this chat
/// * What was the maximum score this user has hit
#[throws(LeetBotError)]
async fn handle_stats_command(msg: &Message) {
    let user_id = i64::from(msg.from.to_user_id());
    let chat_id = i64::from(msg.chat.id());
    info!("Handling /stats command in chat {} from user {}", chat_id, user_id);

    let user = database::get_user_by_id(user_id)?;
    if user.is_none() {
        BOT_API.send(msg.text_reply("Sorry, you were never l33t")).await?;
        return;
    }

    let user = user.unwrap();
    let tz: Option<Tz> = user.timezone.as_ref().map(|it| it.parse::<Tz>().expect("Timezone must be IANA-compliant"));

    let in_tz = |val| {
        tz
            .map(|it| it.from_utc_datetime(&val))
            .map(|it| it.get_leet_date_time())
            .unwrap_or(val.to_string())
    };

    let scores = database::get_scores_for_user(user_id)?;
    if scores.is_empty() {
        BOT_API.send(msg.text_reply("Sorry, you were never l33t")).await?;
        return;
    }

    let mut stats_string = String::from(format!("Stats for user [{}](tg://user?id={}):\n\n", user.username, user.id));

    // created info
    let created = in_tz(user.created_at);
    stats_string.push_str(&format!("\\* User created: *{}*\n", created));

    // updated info
    let updated = in_tz(user.updated_at);
    stats_string.push_str(&format!("\\* User updated: *{}*\n", updated));

    // total for all chats
    let total_win_all = scores.iter().map(|it| it.score).sum::<i64>();
    stats_string.push_str(&format!("- Total score in all chats: *{}*\n", total_win_all));

    // total for this chat
    let total_win_chat = scores.iter().filter(|it| it.chat_id == chat_id).map(|it| it.score).sum::<i64>();
    stats_string.push_str(&format!("- Total score in this chat: *{}*\n", total_win_chat));

    // first score
    let first_win = scores.iter().min_by_key(|it| it.message_timestamp).expect("Scores must not be empty");
    let first_win_date = in_tz(first_win.message_timestamp);
    stats_string.push_str(&format!("- First score: {} with *{}*\n", first_win_date, first_win.score));

    // last score
    let last_win = scores.iter().max_by_key(|it| it.message_timestamp).expect("Scores must not be empty");
    let last_win_date = in_tz(last_win.message_timestamp);
    stats_string.push_str(&format!("- Last score: {} with *{}*\n", last_win_date, last_win.score));

    // max score
    let max_win = scores.iter().max_by_key(|it| it.score).expect("Scores must not be empty");
    let max_win_date = in_tz(max_win.message_timestamp);
    stats_string.push_str(&format!("- Max score: {} with *{}*", max_win_date, max_win.score));

    // send the stats message
    let response = BOT_API.send(msg.text_reply(stats_string).parse_mode(ParseMode::Markdown)).await?;
    let delete_message_id = response.to_message_id();
    info!("Sent /stats command response, message {}", delete_message_id);
}

/// Checks whether message contains l33t time score attempt
///
/// The special case here is that this can be any chat message, not
/// necessarily containing mention of this bot or slash-command.
///
/// The scorers are responsible for recognizing whether the time
/// is "elite" enough or not. Valid scores are described in [matchers] module.
///
/// If score is successful, the bot notifies user about it and schedules a
/// special task to delete its own message after next minute is started, not to
/// pollute chat.
///
/// **Arguments**:
/// * `msg`: Message that this bot received, possible leet score
/// * `text`: Text of the message, must contain partial timestring to be valid score
///
/// **Returns**: `Some(ScoreInfo)` in case user managed to score, `None` otherwise
#[throws(LeetBotError)]
async fn handle_time_match(msg: &Message, text: &str) -> Option<ScoreInfo> {
    // only check text that contains digits
    if text.chars().filter(|c| c.is_numeric()).count() < 4 {
        return None;
    }

    let saved_user = database::get_user(msg)?;
    let sent_time = NaiveDateTime::from_timestamp_opt(msg.date, 0).expect("Message date must be valid");

    let attempt = current_instant_for_user(&saved_user, &sent_time);
    let win = check_match_instant(attempt.as_ref(), text);

    if win > 0 {
        // only save user to database if they scored something
        let mut user = match saved_user {
            Some(saved_user) => saved_user,
            None => {
                let new_user_info = UserInfo::new(i64::from(msg.from.id), &get_username(msg));
                database::insert_user(&new_user_info)?;
                new_user_info
            }
        };

        debug!("Checking user {} for fraud", user.username);
        if check_user_fraud(&user) {
            return None;
        }

        info!("User {} just scored {} at {}", user.username, win, attempt);

        // save updated win strike
        user.total_score += win as i64;
        user.updated_at = Local::now().naive_utc();
        database::update_user(&user)?;

        // save score
        let chat_id = msg.chat.id();
        let score = database::insert_score(&user, i64::from(chat_id), text, sent_time, win)?;

        // send response to the same chat from where we received the message
        let response = BOT_API
            .send(
                msg.text_reply(format!(
                    "[{}](tg://user?id={}) just scored **{}**!\nPrecise time {}",
                    &msg.from.first_name, msg.from.id, win, &attempt.get_leet_date_time()
                ))
                .parse_mode(ParseMode::Markdown),
            )
            .await?;

        // schedule a job that deletes praise message at the start of the next minute
        let delete_message_id = response.to_message_id();
        tokio::spawn(async move {
            wait_till_next_minute_start(Local::now()).await;
            let delete_response = BOT_API.send(response.delete()).await;
            match delete_response {
                Ok(_) => info!("Successfully deleted match message {}", delete_message_id),
                Err(error) => warn!("Couldn't delete match message {}: {:?}", delete_message_id, error),
            }
        });

        return Some(score);
    }

    // user haven't scored, but text contains digits, probably wrong timezone?
    handle_possible_tz_match(&msg, text).await;

    // or maybe he's just late?
    handle_possible_late_match(&msg, text).await;

    return None;
}

/// Retrieves timestring for given user and time
///
/// In order to process the time properly we need to apply timezone to it,
/// i.e., the time must look "elite" to any user in any part of the planet,
/// that's why bot must handle "/setloc" call for setting a timezone for this user.
///
/// **Arguments**:
/// * `user`: user retrieved from database,
///           possibly with `timezone` field set to their current timezone.
/// * `time`: timestamp of the message to convert
///
/// **Returns**: An instance of [LeetDate](utils/LeetDate.html) deducted from
/// user timezone and message time.
///
/// **See also**:
/// * [handle_zone_set](fn.handle_zone_set.html)
fn current_instant_for_user(user: &Option<UserInfo>, time: &NaiveDateTime) -> Box<dyn LeetDate> {
    // add check for timezone for selected user
    if let Some(user) = user {
        // have user
        return current_instant_for_user_timezone(user, time);
    }

    // have no user
    let sent = Local.from_utc_datetime(time);
    return Box::new(sent);
}

fn current_instant_for_user_timezone(user: &UserInfo, time: &NaiveDateTime) -> Box<dyn LeetDate> {
    if let UserInfo { timezone: Some(timezone), .. } = user {
        // have both user and timezone
        let tz: Tz = timezone.parse().expect("Extracted timezone must be IANA-compliant");
        let sent = tz.from_utc_datetime(time);
        return Box::new(sent);
    }

    // user has no timezone
    let sent = Local.from_utc_datetime(time);
    return Box::new(sent);
}

/// This routine checks for attempts at cheating
///
/// As our API is mostly stateless, we need a way to check
/// if users are trying to score by posting duplicate messages to the chat.
/// Here we're checking this by comparing last score time to current timestamp
/// and rejecting the attempt is it is less than a minute far from last
/// successful score for the same user.
///
/// **Arguments**:
/// * `user`: user information for a person who's trying to score right now
///
/// **Returns**: `true` if the user is trying to cheat, `false` otherwise
fn check_user_fraud(user: &UserInfo) -> bool {
    let scores = database::get_scores_for_user(user.id);
    if let Err(error) = scores.as_ref() {
        warn!("Error while checking user for fraud: {}", error);

        // better safe than sorry
        return true;
    }

    // get latest score
    let scores = scores.expect("Error check was already done");
    let most_recent_saved_score = scores.into_iter().max_by_key(|score| score.message_timestamp);

    // compare latest score time to the current time, if it's less than 60 seconds the score is invalid
    // and the user is cheating
    if let Some(most_recent_score) = most_recent_saved_score {
        let now = Local::now().naive_utc();
        let last_scored = most_recent_score.message_timestamp;
        let passed = now.signed_duration_since(last_scored);
        if passed.num_seconds() < 60 {
            warn!("User {} is suspected fraud, last score {}, current time {}", user.username, last_scored, now);
            return true;
        }
    }

    return false;
}

/// Runs matchers on it and produces
/// a score of how much this time is "elite".
///
/// This is the most valuable function for this bot.
///
/// 4 is commonly scored, 6 is rare to get, values above 8
/// are once-in-a-year experience, and everything above 10 is
/// once-in-a-lifetime get.
///
/// **Arguments**:
/// * `attempt`: time of the message converted to timestring
/// * `text`: text of the message
///
/// **Returns**: elite score for `attempt` and `text` combination
fn check_match_instant(attempt: &dyn LeetDate, text: &str) -> i64 {
    let attempt_match = attempt.get_leet_match();
    let attempt_text = longest_common_substring(text, &attempt_match);
    if !attempt_text.contains(&attempt.get_leet_hhmm()) {
        // there's no hhmm leet text in the message, skip
        return 0;
    }

    let attempt_len = attempt_text.chars().count();
    let score = matchers::DEFAULT_MATCH_ROUTINES
        .iter() // from all matchers
        .map(|matcher| matcher(&attempt_match)) // calculate scores
        .max() // select maximum score
        .unwrap_or(0) // shouldn't happen
        .min(i64::try_from(attempt_len).unwrap()); // if text is shorter, use its len

    return score;
}

#[cfg(test)]
mod test {
    use super::*;
    
    #[test]
    #[throws(LeetBotError)]
    fn smoke() {
        let all_scores_for_chat = database::get_scores_for_chat(-1001126381629)?;
        attach_stat_chart(&all_scores_for_chat);
    }
}
