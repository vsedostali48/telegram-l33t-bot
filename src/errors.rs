use std::error;
use std::fmt;

pub use fehler::{throw, throws};

#[derive(Debug)]
pub enum LeetBotError {
    BotApi(telegram_bot::Error),
    Database(diesel::result::Error),
    RenderError(charming::EchartsError),
    UnknownError(String),
}

impl From<telegram_bot::Error> for LeetBotError {
    fn from(error: telegram_bot::Error) -> Self {
        LeetBotError::BotApi(error)
    }
}

impl From<charming::EchartsError> for LeetBotError {
    fn from(error: charming::EchartsError) -> Self {
        LeetBotError::RenderError(error)
    }
}

impl From<diesel::result::Error> for LeetBotError {
    fn from(error: diesel::result::Error) -> Self {
        LeetBotError::Database(error)
    }
}

impl<T> From<tokio::sync::mpsc::error::SendError<T>> for LeetBotError {
    fn from(error: tokio::sync::mpsc::error::SendError<T>) -> Self {
        LeetBotError::UnknownError(error.to_string())
    }
}

impl fmt::Display for LeetBotError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self {
            LeetBotError::BotApi(error) => write!(f, "Bot api error: {}", error),
            LeetBotError::Database(error) => write!(f, "Database error: {}", error),
            LeetBotError::RenderError(error) => write!(f, "Render error: {:?}", error),
            LeetBotError::UnknownError(error) => write!(f, "Unknown error: {}", error),
        }
    }
}

impl error::Error for LeetBotError {}
