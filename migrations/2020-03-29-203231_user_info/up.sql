-- Your SQL goes here
CREATE TABLE users(
    `id` BIGINT PRIMARY KEY NOT NULL,
    `username` TEXT NOT NULL,
    `created_at` TIMESTAMP NOT NULL, -- UTC
    `updated_at` TIMESTAMP NOT NULL, -- UTC
    `total_score` BIGINT NOT NULL DEFAULT 0,
    `timezone` TEXT DEFAULT NULL
)